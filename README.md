# Todo App

> Todo list for FULL STACK DEVELOPER position in Torre Capital

API Docs: https://documenter.getpostman.com/view/160273/TVzYguQA

# Install

```sh
$ yarn
$ yarn build
$ yarn start

# Development
$ yarn debug # Debugging
$ yarn dev # Watched dev mode
```

# Summary

- **`todo-api`** - API  
- **`todo-frontend`** - Frontend
