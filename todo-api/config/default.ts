require("dotenv").config();

const config = {
  port: 3021,
  mongodb: {
    url: "mongo://localhost/todo-list"
  },
  todos: {
    depth: 1
  },
  api: {
    defaultQueryFilters: ["page", "limit", "sort", "skip", "pagination"],
    paginate: {
      max: 100,
      default: 20,
    }
  }
};

declare type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};

export type Config = DeepPartial<typeof config>;
export default config;
