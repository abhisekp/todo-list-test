declare namespace Express {
  export interface Request {
    authorized?: boolean;
    user?: any
  }
}
