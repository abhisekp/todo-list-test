import { Schema } from 'mongoose'

export interface ITodo {
  task: string;
  parent?: ID;
  parents?: (ID | Schema.Types.ObjectId)[];
  completed?: boolean;
  completedAt?: Date;
  createdAt: Date;
  updatedAt: Date;
}
