export interface PaginateOption {
  max: number;
  default: number;
}
