export interface QueryParams<Query = any> {
  query?: Query;
  mongoose?: any;
  [key: string]: any;
}
