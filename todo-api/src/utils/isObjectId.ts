import { Types } from 'mongoose';

const { ObjectId } = Types;

export const isObjectId = (value: any) => {
  try {
    // @ts-ignore
    return String(ObjectId(value)) === String(ObjectId(value));
  } catch (e) {
    return false;
  }
};
