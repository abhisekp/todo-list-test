export const getPage = (page?: number) => {
  if (!page) {
    return 1;
  }

  return Math.max(1, page);
}
