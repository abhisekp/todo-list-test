import * as _ from "lodash/fp";
import config from 'config';

const defaultFilters = config.get<string[]>('api.defaultQueryFilters');

export const filterQuery = (queryFromParams: any = {}) => {
  const filters = _.pickBy(
    (v, k) => k.startsWith("$") || defaultFilters.includes(k),
    queryFromParams
  );
  const query = _.pickBy((v, k) => !k.startsWith("$"), queryFromParams);
  return { filters, query };
};
