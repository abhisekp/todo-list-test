import config from 'config'
import { PaginateOption }  from '../interfaces' 

const paginate = config.get<PaginateOption>('api.paginate')

export const getLimit = (limit?: number) => {
  if (limit == null) {
    return paginate.default;
  }

  return Math.min(paginate.max, limit);
}
