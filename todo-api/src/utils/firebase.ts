import fs from "fs";
import path from "path";
import fbAdmin from "firebase-admin";

export const registerFirebase = (serviceAccountKey?: string) => {
  const serviceAccount =
    serviceAccountKey ??
    JSON.parse(
      fs.readFileSync(
        path.resolve(process.cwd(), "config/local-serviceAccountKey.json"),
        "utf-8"
      )
    );

  return fbAdmin.initializeApp({
    credential: fbAdmin.credential.cert(serviceAccount)
  });
};
