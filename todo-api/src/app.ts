import express, { Application } from "express";
import helmet from "helmet";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import cors from "cors";
import methodOverride from "method-override";
import { mongooseMiddleware, firebaseMiddleware } from "./middlewares";
import { Server } from "typescript-rest";
import { controllers } from "./modules";
import errorHandler from "errorhandler";
import responseTime from "response-time";

export const app: Application = express();

export const registerApp = async (): Promise<Application> => {
  app.use(methodOverride());
  app.use(responseTime());
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(helmet());
  app.use(mongooseMiddleware());
  app.use(firebaseMiddleware());

  Server.buildServices(app, ...controllers);

  app.use(errorHandler());

  return app;
};
