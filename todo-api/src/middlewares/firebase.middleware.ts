import { registerFirebase, logger } from "../utils";

let fbApp;
export const firebaseMiddleware = () => async (req, res, next) => {
  if (fbApp) return next();
  try {
    fbApp = await registerFirebase();
    logger.info("Connected to Firebase");
  } catch (err) {
    logger.info("Could not register firebase");
    logger.error(err);
  }
  next();
};
