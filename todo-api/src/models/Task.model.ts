import mongoose, { Document, ObjectId, Schema } from "mongoose";
import toJSON from "@meanie/mongoose-to-json";
import mongoosePaginate from "mongoose-paginate-v2";
import mongooseDelete from "mongoose-delete";
import { ITodo } from "../interfaces";

const ObjectId = Schema.Types.ObjectId;

export interface TaskSchema extends ITodo, Document {
  deletedAt: Date;
  deleted: Boolean;
}

export const TaskSchema = new Schema<TaskSchema>(
  {
    task: {
      trim: true,
      type: String,
      required: true
    },
    parents: {
      type: [
        {
          ref: "Task",
          type: ObjectId
        }
      ]
    },
    completed: {
      type: Boolean,
      get(value: boolean) {
        return Boolean(value);
      }
    },
    completedAt: {
      type: Date
    },
    createdBy: ObjectId,
    deletedBy: ObjectId
  },
  {
    timestamps: true,
    toJSON: {
      getters: true,
      virtuals: true
    },
    toObject: {
      getters: true,
      virtuals: true
    }
  }
);

TaskSchema.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true });
TaskSchema.plugin(mongoosePaginate);
TaskSchema.plugin(toJSON);

export const TodoModel = mongoose.model<TaskSchema>(
  "Task",
  TaskSchema,
  "tasks"
);
