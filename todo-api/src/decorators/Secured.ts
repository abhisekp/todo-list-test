import { Errors, PreProcessor } from "typescript-rest";
import * as _ from "lodash/fp";
import { Request, Response } from "express";
import fbAdmin from 'firebase-admin';

const secured = (opts?: { strict: false }) => async (
  req: Request,
  res: Response
) => {
  if (req.headers.authtoken) {
    const user = await fbAdmin.auth().verifyIdToken(req.headers.authtoken as string, true);
    req.user = user;
    req.authorized = true;
  }

  if (opts.strict && !req.authorized) {
    throw new Errors.ForbiddenError("User is not authorized");
  }
};

export const Secured = (opts?: any) => PreProcessor(secured(opts));
