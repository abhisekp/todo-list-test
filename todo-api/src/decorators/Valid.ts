import { Errors, PreProcessor } from "typescript-rest";
import * as _ from "lodash/fp";
import { Request, Response } from "express";
import BaseJoi, { AnySchema, AsyncValidationOptions } from "joi";
import { logger } from "../utils";
import { isObjectId } from "../utils";

/**
 * @example
 * {
 *    id: Joi.ObjectId(),
 * }
 *
 * @param {Root} joi
 * @returns {{value: string, errors: any} | {messages: {"ObjectId.base": string}, type: string, base: StringSchema, validate(value: string, helpers: any): (undefined | {value: string, errors: any})}}
 * @constructor
 */
function ObjectIdExt(joi: BaseJoi.Root): BaseJoi.Extension {
  return {
    type: "ObjectId",
    base: joi.string(),
    messages: {
      isObjectId: '"{{#label}}" must be a valid ObjectId'
    },
    validate(value, helpers) {
      if (isObjectId(value)) return;
      return {
        value,
        errors: helpers.error("isObjectId")
      };
    }
  };
}

export const Joi: BaseJoi.Root = BaseJoi.extend(ObjectIdExt);

const withDefaultValidationOptions = _.defaults({
  stripUnknown: true
});

const validator = (schema: AnySchema, opts?: AsyncValidationOptions) => async (
  req: Request,
  res: Response
) => {
  const { params, body, query } = req;
  try {
    const { params: p, body: b, query: q } = await schema.validateAsync(
      { params, body, query },
      withDefaultValidationOptions(opts)
    );
    req.params = p;
    req.body = b;
    req.query = q;
  } catch (e) {
    logger.info("Validation failed:");
    logger.error(e);
    throw new Errors.BadRequestError(`ValidationError: ${e.message}`);
  }
};

export const Valid = (schema: AnySchema, opts?: AsyncValidationOptions) =>
  PreProcessor(validator(schema, opts));
