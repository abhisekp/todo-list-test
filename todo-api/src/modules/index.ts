import { HealthAPIService } from "./health";
import { TodoAPIService } from "./todo";

export const controllers = [HealthAPIService, TodoAPIService];
