import { Path, GET } from "typescript-rest";

@Path("/health")
export class HealthAPIService {
  @GET
  async status(): Promise<{ status: "OK"; [key: string]: any }> {
    return { status: "OK" };
  }
}
