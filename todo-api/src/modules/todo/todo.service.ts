import mongoose from "mongoose";
import { Errors, Return, ContextRequest } from "typescript-rest";
import { logger, getPage, filterQuery, getLimit } from "../../utils";
import { PaginateResult, PaginateOptions } from "mongoose";
import { ITodo, QueryParams } from "../../interfaces";
import { TodoModel, TaskSchema } from "../../models";
import * as _ from "lodash/fp";
import config from "config";

const MAX_LEVEL = config.get<number>("todos.depth");

export class TodoService {
  async findOrGet(
    id: null,
    params: QueryParams
  ): Promise<PaginateResult<TaskSchema>>;
  async findOrGet(id: ID, params: QueryParams): Promise<TaskSchema>;
  async findOrGet(id: any, params: QueryParams) {
    if (id != null) {
      return await this.checkExistenceAndGetTodo(id);
    }

    const { query, filters } = filterQuery(params.query ?? {});
    const $limit = getLimit(filters.$limit ?? filters.limit);
    const $skip = filters.$skip ?? filters.skip;
    const $paginate = filters.$paginate ?? filters.paginate;
    const $page = getPage(filters.$page ?? filters.page);

    const paginateOptions: PaginateOptions = {
      offset: $skip,
      limit: $limit,
      page: $page
    };

    // @ts-ignore
    const result = await TodoModel.paginate(query, paginateOptions);
    return $paginate === false ? result.docs : result;
  }

  async addTodo(
    data: Partial<ITodo>,
    params: QueryParams
  ): Promise<TaskSchema> {
    await this.assocParent(data);

    if (data.completed && !data.completedAt) {
      data.completedAt = new Date();
    }

    return await TodoModel.create(data);
  }

  async updateTodo(
    id: ID,
    data: Partial<ITodo> & mongoose.UpdateQuery<TaskSchema>,
    params: QueryParams
  ): Promise<TaskSchema> {
    await this.checkExistenceAndGetTodo(id);

    if (data.parent) {
      await this.assocParent(data);
    }

    if (data.completed === false) {
      data.$unset = Object.assign(data.$unset ?? {}, {
        completedAt: 1,
      });
    } else {
      data.completedAt = data.completedAt ?? new Date();
    }

    return await TodoModel.findByIdAndUpdate(id, data, {
      new: true,
      runValidators: true
    });
  }

  async removeTodo(
    id: ID,
    params: QueryParams<{ subtasks: boolean }>
  ): Promise<TaskSchema> {
    const todo = await this.checkExistenceAndGetTodo(id, true);

    const removedTodo = await todo.delete();
    const { subtasks } = params?.query ?? {};
    if (subtasks) {
      // Asyncly delete all subtasks derived from this todo
      TodoModel.deleteMany({ parents: id })
        .then(done => logger.info(done))
        .catch(err => logger.error(err));
    } else {
      // Asyncly associate remove this todo ID as parent of other todos
      TodoModel.updateMany({ parents: id }, { $pullAll: { parents: [id] } })
        .then(done => logger.info(done))
        .catch(err => logger.error(err));
    }

    return removedTodo;
  }

  async restoreTodo(
    id: ID,
    params: QueryParams<{ subtasks: boolean }>
  ): Promise<TaskSchema> {
    const todo = await this.checkExistenceAndGetTodo(id, true);

    // @ts-ignore
    return await todo.restore();
  }

  //-----------------//

  private async checkExistenceAndGetTodo(
    id,
    withDeleted = false
  ): Promise<TaskSchema> {
    const todo = withDeleted
      ? // @ts-ignore
        await TodoModel.findOneWithDeleted({ _id: id })
      : await TodoModel.findById(id);

    if (!todo) {
      throw new Errors.NotFoundError(`Could not find a todo with ID ${id}`);
    }

    return todo;
  }

  private async assocParent(data: Partial<ITodo>): Promise<void> {
    // fetch parent (if provided)
    const parentTodo = await TodoModel.findById(data.parent);

    // validate parent todo
    if (data.parent && !parentTodo) {
      throw new Errors.BadRequestError(
        `Todo parent with id ${data.parent} not found`
      );
    }

    // Check the todo level within the parent (if provided)
    if (parentTodo?.parents && parentTodo?.parents?.length > MAX_LEVEL - 1) {
      throw new Errors.BadRequestError(
        `Todo Level cannot be more than ${MAX_LEVEL} depth.`
      );
    }

    if (data.parent) {
      data.parents = (parentTodo.parents ?? []).concat(data.parent);
    }
  }
}
