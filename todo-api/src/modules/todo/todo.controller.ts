import { Inject } from "typescript-ioc";
import {
  Path,
  GET,
  PathParam,
  ContextRequest,
  POST,
  DELETE,
  QueryParam,
  PATCH
} from "typescript-rest";
import { ITodo, QueryParams } from "../../interfaces";
import { TodoService } from "./todo.service";
import { TaskSchema } from "../../models";
import { Secured } from "../../decorators";
import {
  removeTodoValidator,
  getTodoValidator,
  updateTodoValidator,
  addTodoValidator
} from "./validators";
import { PaginateResult } from "mongoose";

@Path("/todos")
export class TodoAPIService {
  @Inject
  todoService: TodoService;

  @GET
  @Secured(getTodoValidator)
  async getAll(@ContextRequest req): Promise<PaginateResult<TaskSchema>> {
    const params: QueryParams = {
      query: req.query
    };
    return await this.todoService.findOrGet(null, params);
  }

  @GET
  @Path(":id")
  async getById(@PathParam("id") id: ID): Promise<TaskSchema> {
    return await this.todoService.findOrGet(id, {});
  }

  @POST
  @Secured(addTodoValidator)
  async addTodo(body: Partial<ITodo>): Promise<TaskSchema> {
    return await this.todoService.addTodo(body, {});
  }

  @PATCH
  @Path(":id")
  @Secured(updateTodoValidator)
  async updateTodoById(
    @PathParam("id") id: ID,
    body: Partial<ITodo>
  ): Promise<TaskSchema> {
    return await this.todoService.updateTodo(id, body, {});
  }

  @DELETE
  @Path(":id")
  @Secured(removeTodoValidator)
  async removeTodoById(
    @PathParam("id") id: ID,
    @QueryParam("subtasks") subtasks: boolean /* Should delete subtasks? */
  ): Promise<TaskSchema> {
    const params: QueryParams = {
      query: {
        subtasks
      }
    };

    return await this.todoService.removeTodo(id, params);
  }

  @DELETE
  @Path(":id/restore")
  @Secured(removeTodoValidator)
  async restoreTodoById(
    @PathParam("id") id: ID,
  ): Promise<TaskSchema> {
    return await this.todoService.restoreTodo(id, {});
  }
}
