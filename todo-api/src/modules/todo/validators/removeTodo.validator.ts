import { Joi } from "../../../decorators";

export const removeTodoValidator = Joi.object({
  params: Joi.object({
    // @ts-ignore
    id: Joi.ObjectId()
  }),
  query: Joi.object({
    subtasks: Joi.boolean(),
  })
});
