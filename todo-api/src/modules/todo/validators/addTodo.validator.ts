import { Joi } from "../../../decorators";

export const addTodoValidator = Joi.object({
  body: Joi.object({
    task: Joi.string().required(),
    // @ts-ignore
    parent: Joi.ObjectId(),
    completed: Joi.boolean(),
    completedAt: Joi.date(),
  })
});
