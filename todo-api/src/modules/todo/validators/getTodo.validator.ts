import { Joi } from "../../../decorators";

export const getTodoValidator = Joi.object({
  params: Joi.object({
    // @ts-ignore
    id: Joi.ObjectId()
  }),
  query: Joi.object({
    $skip: Joi.number(),
    $page: Joi.number(),
    $limit: Joi.number(),
    $paginate: Joi.boolean()
  })
    .rename("skip", "$skip")
    .rename("limit", "$limit")
    .rename("page", "$page")
    .rename("paginate", "$paginate")
});
