import { Joi } from "../../../decorators";

export const updateTodoValidator = Joi.object({
  params: Joi.object({
    // @ts-ignore
    id: Joi.ObjectId()
  }),
  body: Joi.object({
    task: Joi.string(),
    completed: Joi.boolean(),
    completedAt: Joi.date(),
    // @ts-ignore
    parent: Joi.ObjectId()
  })
});
